export const ADD_BOOK = 'ADD_BOOK';
export const REMOVE_BOOK = 'REMOVE_BOOK';
export const UPDATE_ALL_BOOKS = 'UPDATE_ALL_BOOKS';
