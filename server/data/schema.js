const { gql } = require('apollo-server');

const typeDefs = gql`
    type Book {
        id: Int
        title: String
    }

    type Query {
        books: [Book]
    }

    type Mutation {
        addBook(title: String!): Book
    }
`;

module.exports = typeDefs;
