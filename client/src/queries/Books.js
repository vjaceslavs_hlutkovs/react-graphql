import gql from 'graphql-tag';

const GET_BOOKS = gql`
    query {
        books {
            id,
            title
        }
    }
`;

const ADD_BOOK = gql`
    mutation addBook($title: String!) {
        addBook(title: $title) {
            id,
            title
        }
    }
`;

export {
    GET_BOOKS,
    ADD_BOOK
}
