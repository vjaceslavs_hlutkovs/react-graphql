import {
    addBook,
    removeBook,
    updateAllBooks
} from './book';

import {
    increaseCounter,
    decreaseCounter,
    increaseCounterBy,
    dereaseCounterBy
} from './book';

export {
    addBook,
    removeBook,
    updateAllBooks,
    increaseCounter,
    decreaseCounter,
    increaseCounterBy,
    dereaseCounterBy
}