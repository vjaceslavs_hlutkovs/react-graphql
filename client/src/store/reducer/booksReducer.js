import client from '../../utils/client';
import { ADD_BOOK, REMOVE_BOOK, UPDATE_ALL_BOOKS } from '../action/type';

const initialState = {
    books: []
};

const booksReducer = (state = initialState, action) => {
    const allBooks = state.books;

    switch (action.type) {
        case ADD_BOOK:
            const lastBookID = allBooks[allBooks.length - 1].id;

            if (typeof action.book !== 'undefined') {
                action.book.id = lastBookID + 1;
            }
            let newBook = action.book;

            return { ...state, ...{ newBook } };
        case REMOVE_BOOK:
            const newBooks = state.books.filter(book => book.id !== action.id);
            // Update apollo cache and redux store
            client.writeData({ data: { books: newBooks } });
            return { books: newBooks };
        case UPDATE_ALL_BOOKS:
            if (typeof action.books !== 'undefined') {
                return { ...state, ...{ books: action.books } };
            }

            return state;
        default:
            return state;
        }
};

export default booksReducer;
