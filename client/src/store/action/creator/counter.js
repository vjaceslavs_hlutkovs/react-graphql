import { INCREASE_COUNTER, DECREASE_COUNTER } from '../type';

export const increaseCounter = () => ({
    type: INCREASE_COUNTER
});

export const decreaseCounter = () => ({
    type: DECREASE_COUNTER
});

export const increaseCounterBy = number => ({
    type: INCREASE_COUNTER,
    number
});

export const dereaseCounterBy = number => ({
    type: DECREASE_COUNTER,
    number
});