import { combineReducers } from 'redux';
import counterReducer from './counterReducer';
import booksReducer from './booksReducer';

const counterApp = combineReducers({
    counter: counterReducer,
    books: booksReducer
});

export default counterApp;
