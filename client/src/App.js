import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';

import store from './store';
import client from './utils/client';
import Books from './components/Books';
import AddBook from './components/AddBook';
import './App.css';

class App extends Component {
    render() {
        return (
            <Provider store={ store }>
                <ApolloProvider client={ client }>
                    <div className="App">
                        <header className="App-header">
                            <AddBook />
                            <Books />
                        </header>
                    </div>
                </ApolloProvider>
            </Provider>
        );
    }
}

export default App;
