import { ADD_BOOK, REMOVE_BOOK, UPDATE_ALL_BOOKS } from '../type';

export const addBook = book => ({
    type: ADD_BOOK,
    book
});

export const removeBook = id => ({
    type: REMOVE_BOOK,
    id
});

export const updateAllBooks = books => ({
    type: UPDATE_ALL_BOOKS,
    books
});
