import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { ADD_BOOK, GET_BOOKS } from '../queries/Books';
import Loading from './Loading';

const mapStateToProps = state => ({
    books: state.books.books
});

class AddBook extends Component {
    render() {
        let inputTitle;

        return (
            <Mutation
                mutation={ ADD_BOOK }
                update={ (cache, { data: { addBook } }) => {
                    const { books } = cache.readQuery({ query: GET_BOOKS });
                    cache.writeQuery({
                        query: GET_BOOKS,
                        data: { books: books.concat([addBook]) }
                    });
                } }>
                { (addBook, { loading, error }) => {
                    if (loading) return <Loading />;
                    if (error) return 'error';

                    return (
                        <form onSubmit={ (e) => {
                            e.preventDefault();
                            addBook({ variables: { title: inputTitle.value } });
                            inputTitle = null;
                        } }>
                            <input type="text" ref={ (node) => { inputTitle = node } } />
                            <button type="submit">Add Book</button>
                        </form>
                    );
                } }
            </Mutation>
        )
    }
}

export default connect(mapStateToProps)(AddBook);
