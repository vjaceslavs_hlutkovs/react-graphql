const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },

    entry: {
        app: './client/index.js',
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader'
            },

            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },

            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },

            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader'
                    },
                ],
            }
        ]
    },

    output: {
        path: path.resolve(__dirname, '..', 'client', 'dist'),
        publicPath: '/dist/'
    },

    devServer: {
        contentBase: path.join(__dirname, '..', 'client', 'public'),
        port: 3000,
        publicPath: 'http://localhost:3000/dist/',
        hotOnly: true
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};
