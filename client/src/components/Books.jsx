import { connect } from 'react-redux';
import { removeBook, updateAllBooks } from '../store/action/creator';
import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { GET_BOOKS } from '../queries/Books';
import Loading from './Loading';

const mapStateToProps = state => ({
    counter: state.counter.counter,
    books: state.books.books
});

const mapDispatchToProps = dispatch => ({
    onRemoveBookClick: (id) => {
        dispatch(removeBook(id));
    },

    onAllBooksUpdate: (books) => {
        dispatch(updateAllBooks(books));
    }
});

class Books extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: this.props.books
        }
    }

    render() {
        return (
            <Query query={ GET_BOOKS } onCompleted={ (data) => { this.props.onAllBooksUpdate(data.books) } }>
                { ({ loading, error, data }) => {
                    if (loading) return <Loading />;
                    if (error) return 'error';

                    return (
                        <ul>
                            { this.props.books.map(book => (
                                <li key={ book.id }>{ book.title } <button onClick={ () => this.props.onRemoveBookClick(book.id) }>X</button></li>
                            )) }
                        </ul>
                    );
                } }
            </Query>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Books);
