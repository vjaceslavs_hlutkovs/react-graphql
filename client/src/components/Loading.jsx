import React, { Component } from 'react';
import logo from '../assets/logo.png';

class Loading extends Component {
    render() {
        return (<img src={ logo } className="App-logo" alt="logo" />);
    }
}

export default Loading;
