import { INCREASE_COUNTER, DECREASE_COUNTER } from '../action/type';

const initialState = {
    counter: 0
};

const counterReducer = (state = initialState, action) => {
    let counter;

    switch (action.type) {
    case INCREASE_COUNTER:
        let increaseBy = 1;
        if (typeof action.number !== 'undefined') {
            increaseBy = action.number;
        }
        counter = state.counter + increaseBy;

        return { ...state, ...{ counter } };
    case DECREASE_COUNTER:
        let decreaseBy = 1;
        if (typeof action.number !== 'undefined') {
            decreaseBy = action.number;
        }
        counter = state.counter - decreaseBy;

        if (counter < 0) {
            counter = 0;
        }

        return { ...state, ...{ counter } };
    default:
        return state;
    }
};

export default counterReducer;
