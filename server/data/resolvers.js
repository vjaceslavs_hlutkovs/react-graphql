const fs = require('fs');
const path = require('path');

const books = require('./books.json');

const resolvers = {
    Query: {
        books: () => books,
    },

    Mutation: {
        addBook(_, args) {
            args.id = books.length;
            const booksString = JSON.stringify(books);
            const newBooks = JSON.stringify(args);
            const js = booksString.substring(0, booksString.length - 1);

            fs.writeFile(path.join(__dirname, 'books.json'), `${js},${newBooks}]`, (error) => {
                if (error) return console.log('error', error);
                console.log('book saved to json');
            });

            return args;
        }
    }
};

module.exports = resolvers;
