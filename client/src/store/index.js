import { createStore } from 'redux';
import counterApp from './reducer'

const store = createStore(counterApp);

export default store;
